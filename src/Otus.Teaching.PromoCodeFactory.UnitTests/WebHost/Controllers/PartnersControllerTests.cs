﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers
{
    public class PartnersControllerTests
    {
        private readonly Mock<IRepository<Partner>> _mockPartnersRepository;
        private readonly Mock<ICurrentDateTimeProvider> _mockCurrentDateTimeProvider;
        private readonly PartnersController _partnersController;
        
        public PartnersControllerTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _mockPartnersRepository = fixture.Freeze<Mock<IRepository<Partner>>>();
            _mockCurrentDateTimeProvider = fixture.Freeze<Mock<ICurrentDateTimeProvider>>();

            _partnersController = fixture.Build<PartnersController>()
                .OmitAutoProperties() // only for controller
                .Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsZero_ShouldReturnBadRequestWithErrorMessage()
        {
            // Arrange
            var partnerId = Guid.Empty;
            var partnerPromoCodeLimit = Guid.Empty;
            var partner = PartnerBuilder.CreateBasePartner();
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 0
            };

            _mockPartnersRepository
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            _mockCurrentDateTimeProvider
                .Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 14));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);


            // Assert
            result
                .Should().BeOfType<BadRequestObjectResult>();
            (result as BadRequestObjectResult).Value
                .Should().Be("Лимит должен быть больше 0");
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_HasActiveLimit_ShouldCallGetByIdAsyncMethod()
        {
            // Arrange
            var partnerId = Guid.Empty;
            var request = new SetPartnerPromoCodeLimitRequest();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);


            // Assert
            _mockPartnersRepository
                .Verify(x => x.GetByIdAsync(partnerId), Times.Once);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotFound_ShouldReturnNotFound()
        {
            var partnerId = Guid.Empty;
            var request = new SetPartnerPromoCodeLimitRequest();

            _mockPartnersRepository
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(default(Partner));


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);


            // Assert
            result
                .Should().BeOfType<NotFoundResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_HasActiveLimit_ShouldNullifyNumberIssuedPromoCodes()
        {
            // Arrange
            var numberIssuedPromoCodes = 100;
            var partnerId = Guid.Empty;
            var partnerPromoCodeLimit = Guid.Empty;
            var partner = PartnerBuilder
                .CreateBasePartner()
                .WithOnlyOneActiveLimit()
                .WithNumberIssuedPromoCodes(numberIssuedPromoCodes);
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = new DateTime(2022, 01, 01)
            };

            _mockPartnersRepository
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            _mockCurrentDateTimeProvider
                .Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 14));


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);


            // Assert
            partner.NumberIssuedPromoCodes
                .Should().Be(0);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_HasNotActiveLimit_ShouldNotNullifyNumberIssuedPromoCodes()
        {
            // Arrange
            var numberIssuedPromoCodes = 100;
            var partnerId = Guid.Empty;
            var partnerPromoCodeLimit = Guid.Empty;
            var partner = PartnerBuilder
                .CreateBasePartner()
                .WithNotActiveLimit()
                .WithNumberIssuedPromoCodes(numberIssuedPromoCodes);
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = new DateTime(2022, 01, 01)
            };

            _mockPartnersRepository
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            _mockCurrentDateTimeProvider
                .Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 14));


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);


            // Assert
            partner.NumberIssuedPromoCodes
                .Should().Be(numberIssuedPromoCodes);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_HasActiveLimit_ShouldDisablePreviousLimit()
        {
            // Arrange
            var partnerId = Guid.Empty;
            var partnerPromoCodeLimitId = Guid.NewGuid();
            var partner = PartnerBuilder
                .CreateBasePartner()
                .WithOnlyOneActiveLimit();
            var limit = partner.PartnerLimits.First();
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = new DateTime(2022, 01, 01)
            };

            _mockPartnersRepository
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            _mockCurrentDateTimeProvider
                .Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 14));


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);


            // Assert
            limit.CancelDate
                .Should().NotBeNull();
            partner.PartnerLimits
                .Should().HaveCount(2);
            partner.PartnerLimits.Where(x => x.Id == partnerPromoCodeLimitId)
                .Should().NotBeNull();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_HasActiveLimit_ShouldAddNewLimitAndSaveInDB()
        {
            // Arrange
            var partnerId = Guid.Empty;
            var partnerPromoCodeLimitId = Guid.Empty;
            var partner = PartnerBuilder
                .CreateBasePartner();
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = new DateTime(2022, 01, 01)
            };

            _mockPartnersRepository
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            _mockCurrentDateTimeProvider
                .Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 14));


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);


            // Assert
            partner.PartnerLimits
                .Should().HaveCount(2);
            partner.PartnerLimits.Where(x => x.Id == partnerPromoCodeLimitId)
                .Should().NotBeNull();
            _mockPartnersRepository
                .Verify(x => x.UpdateAsync(partner));
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ShouldReturnBadRequestWithTextMessage()
        {
            // Arrange
            var partnerId = Guid.Empty;
            var partnerPromoCodeLimit = Guid.Empty;
            var partner = PartnerBuilder
                .CreateBasePartner()
                .SetAsNotActive();
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = new DateTime(2021, 01, 01)
            };

            _mockPartnersRepository
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);         


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);


            // Assert
            result
                .Should().BeOfType<BadRequestObjectResult>();
            (result as BadRequestObjectResult).Value
                .Should().Be("Данный партнер не активен");
        }

        [Fact]
        public async Task CancelPartnerPromoCodeLimitAsync_HasActiveLimit_ShouldSetCancelDateNow()
        {
            //Arrange
            var partner = PartnerBuilder
                .CreateBasePartner()
                .WithOnlyOneActiveLimit();
            var targetLimit = partner.PartnerLimits.First();
            var partnerId = partner.Id;
            var now = new DateTime(2020, 10, 14);

            var expected = new PartnerPromoCodeLimit()
            {
                Id = targetLimit.Id,
                CreateDate = targetLimit.CreateDate,
                CancelDate = now,
                EndDate = targetLimit.EndDate,
                Limit = targetLimit.Limit
            };

            _mockPartnersRepository
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            
            _mockCurrentDateTimeProvider
                .Setup(x => x.CurrentDateTime)
                .Returns(now);


            // Act
            await _partnersController.CancelPartnerPromoCodeLimitAsync(partnerId);


            // Assert
            targetLimit
                .Should().BeEquivalentTo(expected);
        }

        [Fact]
        public async Task CancelPartnerPromoCodeLimitAsync_PartnerNotFound_ShouldReturnNotFoundWithTextMessage()
        {
            //Arrange
            var partnerId = Guid.Empty;

            _mockPartnersRepository
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(default(Partner));


            // Act
            var result = await _partnersController.CancelPartnerPromoCodeLimitAsync(partnerId);


            // Assert
            result
                .Should().BeOfType<NotFoundObjectResult>();
            (result as NotFoundObjectResult).Value
                .Should().Be("Партнер не найден");
        }

        [Fact]
        public async void CancelPartnerPromoCodeLimitAsync_PartnerIsNotActive_ShouldReturnBadRequestWithTextMessage()
        {
            // Arrange
            var partnerId = Guid.Empty;
            var partnerPromoCodeLimit = Guid.Empty;
            var partner = PartnerBuilder
                .CreateBasePartner()
                .SetAsNotActive();
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = new DateTime(2021, 01, 01)
            };

            _mockPartnersRepository
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);


            // Act
            var result = await _partnersController.CancelPartnerPromoCodeLimitAsync(partnerId);


            // Assert
            result
                .Should().BeOfType<BadRequestObjectResult>();
            (result as BadRequestObjectResult).Value
                .Should().Be("Партнер заблокирован");
        }
    }
}